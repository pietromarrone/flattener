# PagoPA: Senior Software Engineer

## Test 1: Write some code that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4]

This is an Helper Library to Flattern any nested Array of Integers
It has overwritten method: with default max deep, and providing max deep

### How to use

```
import static it.pagopa.flattern.Flatner.flat;
...
...
Object[] toFlat = new Object[] { 1, 2, new Object[] { 3, 4, new Object[] { 5 }, 6, 7 }, 8, 9, 10 };
...
// Perform flattener with default MAX_DEEP (3000)
Integer[] flattened = flat(toFlat);

// or Perform flattener with specified MAX_DEEP (10)
Integer[] flattened = flat(toFlat, 10L);
```

### Continuous Integration

On every commit Gitlab perform these Steps:

- build
- test
- package

### Open Issue

- On very deep nested array (more 4000 nodes) StackOverflowError arise: To improve nesting capability should investigate the solution proposed here https://freecontent.manning.com/stack-safe-recursion-in-java/
