package it.pagopa.flattern;

import static it.pagopa.flattern.Flatner.flat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FlattenerTest {

  final Integer[] EXPECTED = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

  @Test
  public void test_Empty_ShouldReturnEmpty() {
    Object[] toTest = new Object[] {};
    assertArrayEquals(new Integer[] {}, flat(toTest), "Empty shoud return Empty");
  }

  @Test
  public void test_Flat_ShouldReturnExpected() {
    Object[] toTest = EXPECTED;
    assertArrayEquals(EXPECTED, flat(toTest), "Should return the Expected Array");
  }

  @Test
  public void test_OneLevelNested_ShouldReturnExpected() {
    Object[] toTest = new Object[] { 1, 2, 3, 4, new Object[] { 5, 6, 7, 8 }, 9, 10 };
    assertArrayEquals(EXPECTED, flat(toTest), "Should return the Expected Array");
  }

  @Test
  public void test_OneLevelNestedWithDuplicateElements_ShouldReturnExpected() {
    Object[] toTest = new Object[] { 1, 1, new Object[] { 1, 1 } };
    assertArrayEquals(new Integer[] { 1, 1, 1, 1 }, flat(toTest), "Should return the Expected Array");
  }

  @Test
  public void test_MultipleNested_ShouldReturnExpected() {
    Object[] toTest = new Object[] { 1, 2, new Object[] { 3, 4, new Object[] { 5 }, 6, 7 }, 8, 9, 10 };
    assertArrayEquals(EXPECTED, flat(toTest), "Should return the Expected Array");
  }

  @Test
  public void test_MultipleNested_WithMaxDeepLower_ShouldReturnIllegalArgumentException() {
    Object[] toTest = new Object[] { 1, 2, new Object[] { 3, 4, new Object[] { 5 }, 6, 7 }, 8, 9, 10 };
    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      flat(toTest, 2L);
    }, "Max DEEP is lower than Nested Array, IllegalArgumentException should be Thrown");
  }

  @Test
  public void test_ObjectInArray_ShouldThrowIllegalArgumentException() {
    Object[] toTest = new Object[] { new Object() };
    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      flat(toTest);
    }, "Objects in Array are not allowed, IllegalArgumentException should be Thrown");
  }

  @Test
  public void test_Null_ShouldThrowIllegalArgumentException() {
    Object[] toTest = null;
    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      flat(toTest);
    }, "Null in Array are not allowed, IllegalArgumentException should be Thrown");
  }

  @Test
  public void test_NullInArray_ShouldThrowIllegalArgumentException() {
    Object[] toTest = new Object[] { null };
    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      flat(toTest);
    }, "Null in Array are not allowed, IllegalArgumentException should be Thrown");
  }

  @Test
  public void test_NullInNestedArray_ShouldThrowIllegalArgumentException() {
    Object[] toTest = new Object[] { 1, new Object[] { 2, null } };
    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      flat(toTest);
    }, "Objects in Nested Array are not allowed, IllegalArgumentException should be Thrown");
  }

  @Test
  public void test_CircularReference_WhitDefaultLimit_ShouldThrowIllegalArgumentException() {
    Object[] toTest = new Object[] { 1, 2 };
    Object[] circularReference = new Object[] { 3, toTest };
    toTest[0] = circularReference;

    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      flat(toTest);
    }, "Max Deep, IllegalArgumentException should be Thrown");
  }

  @Test
  public void test_CircularReference_WithHighDeep_ShouldThrowStackOverflowError() {
    Object[] toTest = new Object[] { 1, 2 };
    Object[] circularReference = new Object[] { 3, toTest };
    toTest[0] = circularReference;

    Assertions.assertThrows(StackOverflowError.class, () -> {
      flat(toTest, Long.MAX_VALUE);
    }, "Objects in Array are not allowed, StackOverflowError should be Thrown");
  }

}
