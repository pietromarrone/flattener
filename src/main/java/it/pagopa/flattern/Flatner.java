package it.pagopa.flattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class consists of {@code static} utility methods for operating on nested
 * Arrays of {@code Integer}.
 *
 * @since 1.0
 * @author pietromarrone
 */
public class Flatner {

  /**
   * Default Max deep of Array alloewd
   */
  final static Long MAX_DEEP = 3000L;

  // Suppresses default constructor, ensuring non-instantiability.
  private Flatner() {
  }

  /**
   * Flatten the input array of arbitrarily nested arrays of integers into a flat
   * array, with max Deep to 3000
   *
   * @param elements: the input array of arbitrarily nested arrays of integers
   * @return the Flat Array of Integers
   * @throws IllegalArgumentException if elements are not Integer or Nested Arrays
   */
  public static Integer[] flat(Object[] elements) throws IllegalArgumentException {
    return flat(elements, new ArrayList<Integer>(), MAX_DEEP);
  }

  /**
   * Flatten the input array of arbitrarily nested arrays of integers into a flat
   * array. Client indicates the max number of deeps, if to much may throws
   * {@code StackOverflowError}
   *
   * @param deep:     {@code Long} maxumum array deep allowed
   * @param elements: the input array of arbitrarily nested arrays of integers
   * @return the Flat Array of Integers
   * @throws IllegalArgumentException if elements are not Integer or Nested Arrays
   */
  public static Integer[] flat(Object[] elements, Long deep) throws IllegalArgumentException {
    return flat(elements, new ArrayList<Integer>(), deep);
  }

  private static Integer[] flat(Object[] elements, List<Integer> flattened, Long deep) throws IllegalArgumentException {

    // Check Pre-condition
    if (Objects.isNull(elements)) {
      throw new IllegalArgumentException("Input must be Integers or nested array of Integers");
    }

    if (deep <= 0) {
      throw new IllegalArgumentException("the depth of the array greater than max allowed");
    }

    // Cycle Array
    for (Object element : elements) {
      if (isElementAllowed(element)) {
        flattened.add((Integer) element);

      } else if (isIterable(element)) {
        // Is Iterable, Cast it and recurse
        flat((Object[]) element, flattened, --deep);

      } else {
        // The Element is Neither Iterable nor Acceptable so Throws Exception
        throw new IllegalArgumentException("Input must be an array of Integers or nested arrays of Integers");
      }
    }

    return flattened.toArray(new Integer[] {});
  }

  /**
   * Checks if item is Not null, and Array
   *
   * @param item a reference to be checked if is Iterable
   * @return {@code true} if item is an Array otherwise {@code false}
   */
  private static boolean isIterable(Object item) {
    // null control for performance reasons, although redundant
    return Objects.nonNull(item) && item instanceof Object[];
  }

  /**
   * Checks if the item is Allowed as final flat Array
   *
   * @param item a reference to be checked if is Allowed as leaf
   * @return {@code true} if item is an Integer otherwise {@code false}
   */
  private static boolean isElementAllowed(Object item) {
    // null control for performance reasons, although redundant
    return Objects.nonNull(item) && item instanceof Integer;
  }

}
